package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

// словарь для конвертации в арабские
var dict = map[string]int{
	"1":    1,
	"2":    2,
	"3":    3,
	"4":    4,
	"5":    5,
	"6":    6,
	"7":    7,
	"8":    8,
	"9":    9,
	"10":   10,
	"I":    1,
	"II":   2,
	"III":  3,
	"IV":   4,
	"V":    5,
	"VI":   6,
	"VII":  7,
	"VIII": 8,
	"IX":   9,
	"X":    10,
}

// функция проверки соответствия чисел требованиям.
func checkCondition(s string, m map[string]int) {
	var tmp bool = false
	for key := range m {
		if key == s {
			tmp = true
		}
	}
	if !tmp {
		log.Fatal("Ошибка! Не корректный формат чисел.")
	}
}

// функция проверки типа чисел и ковертации sring в int
func convIn(s string, m map[string]int) (int, bool) {
	flag := false
	for range m {
		if s == "1" || s == "2" || s == "3" || s == "4" || s == "5" || s == "6" || s == "7" || s == "8" || s == "9" || s == "10" {
			flag = true
		}
	}
	return m[s], flag
}

// функция ковертации в римские числа
func convOut(num int) string {
	var res string
	var num1, num2, num3 int

	if num <= 9 {
		num1 = 0
		num2 = 0
		num3 = num
	} else if num >= 10 && num <= 19 {
		num1 = 0
		num2 = 10
		num3 = num - num2
	} else if num >= 20 && num <= 29 {
		num1 = 0
		num2 = 20
		num3 = num - num2
	} else if num >= 30 && num <= 39 {
		num1 = 0
		num2 = 30
		num3 = num - num2
	} else if num >= 40 && num <= 49 {
		num1 = 0
		num2 = 40
		num3 = num - num2
	} else if num >= 50 && num <= 59 {
		num1 = 0
		num2 = 50
		num3 = num - num2
	} else if num >= 60 && num <= 69 {
		num1 = 0
		num2 = 60
		num3 = num - num2
	} else if num >= 70 && num <= 79 {
		num1 = 0
		num2 = 70
		num3 = num - num2
	} else if num >= 80 && num <= 89 {
		num1 = 0
		num2 = 80
		num3 = num - num2
	} else if num >= 90 && num <= 99 {
		num1 = 0
		num2 = 90
		num3 = num - num2
	} else if num == 100 {
		num1 = 100
		num2 = 0
		num3 = 0
	}

	if num1 == 100 {
		res += "C"
	}

	switch num2 {
	case 10:
		res += "X"
	case 20:
		res += "XX"
	case 30:
		res += "XXX"
	case 40:
		res += "XL"
	case 50:
		res += "L"
	case 60:
		res += "LX"
	case 70:
		res += "LXX"
	case 80:
		res += "LXXX"
	case 90:
		res += "XC"
	}

	switch num3 {
	case 1:
		res += "I"
	case 2:
		res += "II"
	case 3:
		res += "III"
	case 4:
		res += "IV"
	case 5:
		res += "V"
	case 6:
		res += "VI"
	case 7:
		res += "VII"
	case 8:
		res += "VIII"
	case 9:
		res += "IX"
	}

	return res

}

func main() {

	fmt.Println("КАЛЬКУЛЯТОР \nВозможен ввод римских (I-X) и арабских чисел (1-10). \nПример формата ввода: 1 + 2 или V * VII")
	fmt.Print("Введите задачу: ")

	//чтение ввода с консоли
	reader := bufio.NewReader(os.Stdin)
	in, _ := reader.ReadString('\n')

	task := strings.Fields(in) // Разбивает строку на подсроки, разделенные " ". Записывает в срез

	if len(task) > 3 || len(task) < 3 {
		log.Fatal("Ошибка! Не соответствие формата ввода. Неверное кол-во операндов.")
	}

	checkCondition(task[0], dict)
	checkCondition(task[2], dict)
	if task[1] != "+" && task[1] != "-" && task[1] != "*" && task[1] != "/" {
		log.Fatal("Ошибка! Не корректный оператор.")
	}

	num1, flag1 := convIn(task[0], dict)
	num2, flag2 := convIn(task[2], dict)

	var flag bool
	if flag1 != flag2 {
		log.Fatal("Ошибка! Не соответствие формата ввода. Использованы разные системы счисления.")
	} else {
		flag = flag1
	}

	if flag {
		var res int
		switch task[1] {
		case "+":
			res = num1 + num2
		case "-":
			res = num1 - num2
		case "*":
			res = num1 * num2
		case "/":
			res = num1 / num2
		}
		fmt.Print("Ответ: ", res)
	} else {
		var res int
		switch task[1] {
		case "+":
			res = num1 + num2
		case "-":
			res = num1 - num2
		case "*":
			res = num1 * num2
		case "/":
			res = num1 / num2
		}

		if res < 1 {
			log.Fatal("Ошибка! Ответ меньше единицы, не предусмотрен римской системой счисления.")

		} else {
			fmt.Print("Ответ: ", convOut(res))
		}

	}

}
